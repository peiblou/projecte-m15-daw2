
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class updatepasswordService {

    URL = "http://localhost/proyecto_m15_daw2/";

    constructor(private http: HttpClient) { }

    UpdatePassword(password) {
        return this.http.post(`${this.URL}updatePassword.php`, JSON.stringify(password), this.generateHeaders());
    }

    generateHeaders() {
        if (localStorage.getItem("token") && localStorage.getItem("token") != "undefined") {
            return {
                headers: new HttpHeaders({
                    //'Content-Type': 'application/json',
                    'Authorization': localStorage.getItem("token")
                })
            };
        } else { return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }; }
    }
}