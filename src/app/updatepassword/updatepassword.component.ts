import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { updatepasswordService } from './updatepassword.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-root',
    templateUrl: './updatepassword.component.html',
    styleUrls: ['./updatepassword.component.css']
})
export class UpdatepasswordComponent {
    title = 'practica06Angular';
    update = {
        contraseya_actual: "",
        nueva_contraseya: ""
    }
    logueado = localStorage.getItem("token");


    constructor(private route: Router,
        private _actRoute: ActivatedRoute,
        private updateServicio: updatepasswordService) { }

    ngOnInit(): void {
        if (this.logueado == null) {
            window.location.href = "/";
        }
    }

    cerrarSesion() {
        localStorage.removeItem("token");
    }

    UpdatePassword() {
        this.updateServicio.UpdatePassword(this.update).subscribe(
            datos => {
                if (datos['resultado'] == 'OK') {
                    Swal.fire('Hey user!', 'Contraseña Actulizada!', 'info');
                    Swal.update({
                        icon: 'success'
                    });
                } else {
                    if (datos['resultado'] == 'FAIL') {
                        Swal.fire('Hey user!', 'Error!', 'info');
                        Swal.update({
                            icon: 'error'
                        });
                    }
                }
            }
        );
    }

}
