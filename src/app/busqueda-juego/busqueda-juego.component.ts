import { Component, HostListener, OnInit } from '@angular/core';
import { Juego } from '../model/juego';
import { JuegoService } from '../model/juego.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-busqueda-juego',
  templateUrl: './busqueda-juego.component.html',
  styleUrls: ['./busqueda-juego.component.css'],
  providers: [JuegoService]
})
export class BusquedaJuegoComponent implements OnInit {

  lista_juegos: Array<Juego>;
  responsive = false;
  logueado = localStorage.getItem("token");
  serverName;

  constructor(private _juegoService: JuegoService, private _actRoute:ActivatedRoute, private router:Router) { }

  texto_nombre;

  ngOnInit(): void {
    this._actRoute.paramMap.subscribe(
      (params) => {
        this.texto_nombre = params.get("filtro");
      }
    );

    this._juegoService.getJuegosFilter(this.texto_nombre)
            .subscribe(
                (result) => {
                    this.lista_juegos = result;
                    if (screen.width < 992) {
                        this.responsive = true;
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    cerrarSesion(){
        localStorage.removeItem("token");
    }
    @HostListener('mouseover', ['$event.target']) onHover(evt) {
        if (evt.classList.contains('imgJuego')) {
            evt.style = "filter: brightness(70%);";
        }
    }

    @HostListener('mouseout', ['$event.target']) onOut(evt) {
        if (evt.classList.contains('imgJuego')) {
            evt.style = "filter: brightness(100%);";
        }
    }

}
