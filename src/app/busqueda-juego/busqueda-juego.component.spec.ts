import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusquedaJuegoComponent } from './busqueda-juego.component';

describe('BusquedaJuegoComponent', () => {
  let component: BusquedaJuegoComponent;
  let fixture: ComponentFixture<BusquedaJuegoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusquedaJuegoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusquedaJuegoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
