import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-root',
    templateUrl: './terminos.component.html',
    styleUrls: ['./terminos.component.css']
})
export class terminosComponent {
    title = 'practica06Angular';
    logueado=localStorage.getItem("token");

    constructor(private http: HttpClient, private router: Router) { }

    comprobarLogin(){
        if(this.logueado==null){
            return false;
        }else{
            return true;
        }
    }
    cerrarSesion(){
        localStorage.removeItem("token");
    }
}