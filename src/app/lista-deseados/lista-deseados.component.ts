import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../model/usuario.service';

@Component({
  selector: 'app-lista-deseados',
  templateUrl: './lista-deseados.component.html',
  styleUrls: ['./lista-deseados.component.css'],
  providers: [UsuarioService]
})
export class ListaDeseadosComponent implements OnInit {

  constructor(private _usuarioService:UsuarioService) { }

  lista_deseados=[];
  responsive=false;

  ngOnInit(): void {
    this._usuarioService.getListaDeseados()
    .subscribe(
      (result) => {
        this.lista_deseados=result;
        if (screen.width < 992) {
          this.responsive = true;
        }
      },
      (error) => {
        console.log(error);
      }
    );  
  }

}
