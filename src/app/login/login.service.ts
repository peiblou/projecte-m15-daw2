import { Injectable, Output, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  redirectUrl: string;

  URL = "http://localhost/proyecto_m15_daw2/";

  constructor(private http: HttpClient) { }

  loginUser(login) {
    return this.http.post(`${this.URL}login.php`, JSON.stringify(login));
  }

  chekToken(): Observable<any> {
    let URL = "http://localhost/proyecto_m15_daw2/";
    return this.http.get(URL, this.generateHeaders());
  }
  generateHeaders() {
    if (localStorage.getItem("token") && localStorage.getItem("token") != "undefined") {
      return {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': localStorage.getItem("token")
        })
      };
    } else { return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }; }
  }

}
