import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-root',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],

})
export class LoginComponent {
  title = 'practica06Angular';

  login = {
    correo: "",
    contraseya: ""
  }
  logueado= localStorage.getItem("token")

  constructor(private loginServicio: LoginService, private http: HttpClient, private router: Router) { }
  ngOnInit() {
    if(this.logueado!=null){
      window.location.href = "";
    }

  }
  loginUser() {
    this.loginServicio.loginUser(this.login).subscribe(
      datos => {
        if (datos['resultado'] == 'OK') {

          Swal.fire('Hey user!', 'Login Exitoso!', 'info');

          Swal.update({
            icon: 'success'
          });
          let token = datos['token'];
          localStorage.setItem("token", token);
          this.router.navigate(['/inicio']);
        } else {
          datos['resultado'] == 'FAIL';
          Swal.fire('Hey user!', 'Login Fallido!', 'info');

          Swal.update({
            icon: 'error'
          })
        }
      }
    );
  }

}
