import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistroComponent } from './registro/registro.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
//import { IndexComponent } from './index/index.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { inicioComponent } from './inicio/inicio.component';
import { LogueadoComponent } from './logueado/logueado.component';
import { updateperfilComponent } from './updateperfil/updateperfil.componet';
import { UpdatepasswordComponent } from './updatepassword/updatepassword.component';
import { terminosComponent } from './terminos/terminos.component';
import { eliminarComponent } from './deletecount/deletecount.component';
import { PaginaJuegoComponent } from './pagina-juego/pagina-juego.component';
import { BusquedaJuegoComponent } from './busqueda-juego/busqueda-juego.component';
import { JuegosPlataformaComponent } from './juegos-plataforma/juegos-plataforma.component';
import { ListaDeseadosComponent } from './lista-deseados/lista-deseados.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    LoginComponent,
    //IndexComponent,
    inicioComponent,
    LogueadoComponent,
    updateperfilComponent,
    UpdatepasswordComponent,
    terminosComponent,
    eliminarComponent,
    PaginaJuegoComponent,
    BusquedaJuegoComponent,
    JuegosPlataformaComponent,
    ListaDeseadosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,

  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
