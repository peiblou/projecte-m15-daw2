import { Injectable, Output, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";


@Injectable({
    providedIn: 'root'
})
export class comentarioService {
    redirectUrl: string;

    URL = "http://localhost/proyecto_m15_daw2/";

    constructor(private http: HttpClient) { }

    comnetarioUser(comentarios) {
        return this.http.post(`${this.URL}comentarioregistro.php`, JSON.stringify(comentarios), this.generateHeaders());
    }

    chekToken(): Observable<any> {
        let URL = "http://localhost/proyecto_m15_daw2/";
        return this.http.get(URL, this.generateHeaders());
    }
    generateHeaders() {
        if (localStorage.getItem("token") && localStorage.getItem("token") != "undefined") {
            return {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Authorization': localStorage.getItem("token")
                })
            };
        } else { return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }; }
    }

}
