import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { comentarioService } from './deletecount.service';
import Swal from 'sweetalert2';


@Component({
    selector: 'app-root',
    templateUrl: './deletecount.component.html',
    styleUrls: ['./deletecount.component.css']
})
export class eliminarComponent {
    title = 'practica06Angular';

    comentario = null;
    nuevosComentarios = {
        contraseya_actual: "",
        //comentarios: "",
    }

    constructor(private route: Router,
        private _actRoute: ActivatedRoute,
        private comentarioServicio: comentarioService, private router: Router) { }

    cerrarSesion() {
        localStorage.removeItem("token");
    }

    comnetarioUser() {
        this.comentarioServicio.comnetarioUser(this.nuevosComentarios).subscribe(
            datos => {
                if (datos['resultado'] == 'OK') {
                    localStorage.removeItem("token");
                    Swal.fire('Hey user!', 'Baja con Exitoso!', 'info');
                    Swal.update({
                        icon: 'success'
                    });
                    this.router.navigate(['/']);
                } else {
                    if (datos['resultado'] == 'FAIL') {
                        Swal.fire('Hey user!', 'Error existe!', 'info');
                        Swal.update({
                            icon: 'error'
                        });
                    }
                    if (datos['resultado'] == 'FAIL2') {
                        Swal.fire('Hey user!', 'Campos vacios o Contraseya Incorrecta!', 'info');
                        Swal.update({
                            icon: 'error'
                        });
                    }
                }
            }
        );
    }
}