import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { registroService } from './registro.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
})
export class RegistroComponent implements OnInit {
  title = 'practica005Angular';
  valorparametre = '';

  usuarios = null;

  nuevoUsuario = {
    idUsuario: 0,
    nombre: "",
    telefono: "",
    correo: "",
    contraseya: "",
  }


  constructor(private route: Router,
    private _actRoute: ActivatedRoute,
    private registroServicio: registroService) { }

  ngOnInit() {
    this._actRoute.paramMap.subscribe(
      (params) => {
        this.valorparametre = params.get("");
      }
    );
  }

  altaRegistro() {
    this.registroServicio.altaRegistro(this.nuevoUsuario).subscribe(
      datos => {
        if (datos['resultado'] == 'OK') {
          Swal.fire('Hey user!', 'Registro Exitoso!', 'info');
          Swal.update({
            icon: 'success'
          });
        } else {
          if (datos['resultado'] == 'FAIL') {
            Swal.fire('Hey user!', 'Este correo Ya existe!', 'info');
            Swal.update({
              icon: 'error'
            });
          }
          if (datos['resultado'] == 'FAIL1') {
            Swal.fire('Hey user!', 'Este Telefono Ya existe!', 'info');
            Swal.update({
              icon: 'error'
            });
          }
          if (datos['resultado'] == 'FAIL2') {
            Swal.fire('Hey user!', 'Este Usuario Ya existe!', 'info');
            Swal.update({
              icon: 'error'
            });
          }
          if (datos['resultado'] == 'FAIL3') {
            Swal.fire('Hey user!', 'Campos Vacios!', 'info');
            Swal.update({
              icon: 'error'
            });
          }
        }
      }
    );
  }
}