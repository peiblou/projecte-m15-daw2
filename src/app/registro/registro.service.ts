import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class registroService {

  URL = "http://localhost/proyecto_m15_daw2/";

  constructor(private http: HttpClient) { }

  altaRegistro(registro) {
    return this.http.post(`${this.URL}altaRegistro.php`, JSON.stringify(registro));
  }
}