import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaJuegoComponent } from './pagina-juego.component';

describe('PaginaJuegoComponent', () => {
  let component: PaginaJuegoComponent;
  let fixture: ComponentFixture<PaginaJuegoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginaJuegoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaJuegoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
