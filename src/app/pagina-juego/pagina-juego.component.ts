import { AfterViewInit, ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JuegoService } from '../model/juego.service';
import { TiendaService } from '../model/tienda.service';
import { PlataformaService } from '../model/plataforma.service';
import { UsuarioService } from '../model/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pagina-juego',
  templateUrl: './pagina-juego.component.html',
  styleUrls: ['./pagina-juego.component.css'],
  providers: [JuegoService, TiendaService, PlataformaService, UsuarioService]
})
export class PaginaJuegoComponent implements OnInit{

  nombre_juego="";
  juego="";
  relacion="";
  lista_tiendas=[];
  lista_plataformas=[];
  precio=[];
  data_cargada=false;
  logueado;
  lista_deseados;
  juego_agregado = false;

  constructor(private router:Router,
    private _actRoute:ActivatedRoute,
    private _juegoService:JuegoService,
    private _tiendaService:TiendaService,
    private _plataformaService:PlataformaService,
    private _usuarioService:UsuarioService,) { }

  
  ngOnInit(): void {
    this._actRoute.paramMap.subscribe(
      (params) => {
        this.nombre_juego = params.get("juego");
      }
    );

    this._juegoService.getJuego(this.nombre_juego)
      .subscribe(
        (result) => {
          this.juego = result[0];
          this.logueado = localStorage.getItem("token");
          if (this.logueado != null) {
            this._usuarioService.getListaDeseados()
            .subscribe(
              (result) => {
                this.lista_deseados = result;
                console.log(this.lista_deseados);
                for(let i=0; i < this.lista_deseados.length; i++){
                  if(this.juego["id_juego"] == this.lista_deseados[i]["id"]){
                    
                    this.juego_agregado = true;
                  
                  }
                }
              },
              (error) => {
                console.log(error);
              }
            );
          }

          this._juegoService.getRelacion(this.juego["id_juego"])
            .subscribe(
              (result) => {
                this.relacion = result;

                for(let i=0; i < this.relacion.length; i++){
                  this.lista_tiendas.push(this.relacion[i]["id_tienda"]);
                  this.lista_plataformas.push(this.relacion[i]["id_plataforma"]);
                }

                this._tiendaService.getTiendas(this.lista_tiendas)
                  .subscribe(
                    (result) => {
                      this.lista_tiendas = result;

                      this._plataformaService.getPlataformas(this.lista_plataformas)
                        .subscribe(
                          (result) => {
                            this.lista_plataformas = result;

                            for(let i=0; i < this.relacion.length; i++){
                              this._juegoService.getPrecio(this.relacion[i]["url"])
                                .subscribe(
                                  (result) => {
                                    this.relacion[i]["precio"]=result;
                                  },
                                  (error) => {
                                    console.log(error);
                                  }
                                );
                            }
                            this.data_cargada = true;
                          },
                          (error) => {
                            console.log(error);
                          }
                        );
                    },
                    (error) => {
                      console.log(error);
                    }
                  );
              },
              (error) => {
                console.log(error);
              }
            );
        },
        (error) => {
          console.log(error);
        }
      ); 
      
      
  }

  ObtenerNombreTienda(id){
    for(let i=0; i<this.lista_tiendas.length; i++){
      if(this.lista_tiendas[i]["id_tienda"] == id){
        return this.lista_tiendas[i]["Nombre"];
      }
    }
  }

  ObtenerImagenTienda(id){
    for(let i=0; i<this.lista_tiendas.length; i++){
      if(this.lista_tiendas[i]["id_tienda"] == id){
        return this.lista_tiendas[i]["Imagen"];
      }
    }
  }

  postListaDeseados(id, nombre, imagen, imagenResponsive){
    this._usuarioService.postJuegoListaDeseados(id, nombre, imagen, imagenResponsive)
      .subscribe(
        datos=>{
          if (datos['resultado'] == 'OK') {
            Swal.fire('Hey user!', 'Agregado a la lista de deseos', 'info');
  
            Swal.update({
              icon: 'success'
            });

            this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
              this.router.navigate(["juego/"+this.nombre_juego]);
            });
          } else {
            datos['resultado'] == 'FAIL';
            Swal.fire('Hey user!', 'No se pudo añadir', 'info');
  
            Swal.update({
              icon: 'error'
            })
          }
        }
      );
      
    
  }

  postQuitarListaDeseados(id){
    this._usuarioService.postQuitarJuegoListaDeseados(id)
      .subscribe(
        datos=>{
          if (datos['resultado'] == 'OK') {
            Swal.fire('Hey user!', 'Eliminado correctamente', 'info');
  
            Swal.update({
              icon: 'success'
            });

            this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
              this.router.navigate(["juego/"+this.nombre_juego]);
            });
          } else {
            datos['resultado'] == 'FAIL';
            Swal.fire('Hey user!', 'No se pudo encontrar o eliminar', 'info');
  
            Swal.update({
              icon: 'error'
            })
          }
        },
      );
    
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(["juego/"+this.nombre_juego]);
    });
  }

}
