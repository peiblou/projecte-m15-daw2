export class Plataforma{
    id_plataforma:number;
    nombre:string;
    imagen:string;

    constructor(id_plataforma:number, nombre:string, imagen:string){
        this.id_plataforma = id_plataforma;
        this.nombre = nombre;
        this.imagen = imagen;
    }
}