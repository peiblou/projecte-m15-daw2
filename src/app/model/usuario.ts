export class Usuario{
    id_usuario:number;
    nombre:string;
    correo:string;
    telefono:number;
    deseados:Array<any>;

    constructor(id_usuario:number, nombre:string, correo:string, telefono:number, deseados:Array<any>){
        this.id_usuario = id_usuario;
        this.nombre = nombre;
        this.correo = correo;
        this.telefono = telefono;
        this.deseados = this.deseados;
    }
}