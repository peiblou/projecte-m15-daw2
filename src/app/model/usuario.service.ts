import { Inject, Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { Usuario } from './usuario';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class UsuarioService {
    constructor(private conexHttp: HttpClient) { }

    getListaDeseados(): Observable<any> {
        let url = "http://localhost/proyecto_m15_daw2/getDeseados.php?";
        return this.conexHttp.get(url, this.generateHeaders() );
    }

    postJuegoListaDeseados(id, nombre, imagen, imagenResponsive): Observable<any>{
        let url = "http://localhost/proyecto_m15_daw2/postJuegoListaDeseados.php?";
        let body:any = {
            "id":id,
            "nombre":nombre,
            "imagen":imagen,
            "imagenResponsive":imagenResponsive
        };
        return this.conexHttp.post(url, body, this.generateHeaders() );
    }

    postQuitarJuegoListaDeseados(id): Observable<any>{
        let url = "http://localhost/proyecto_m15_daw2/postQuitarJuegoListaDeseados.php?";
        let body:any = {
            "id":id,
        };
        return this.conexHttp.post(url, body, this.generateHeaders() );
    }

    generateHeaders() {
        if (localStorage.getItem("token") && localStorage.getItem("token") != "undefined") {
            return {
                headers: new HttpHeaders({
                    //'Content-Type': 'application/json',
                    'Authorization': localStorage.getItem("token")
                })
            };
        } else { return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }; }
    }
}