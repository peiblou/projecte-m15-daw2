export class Juego{
    id_juego:number;
    nombre:string;
    imagen:string;
    imagenResponsive:string;

    constructor(id_juego:number, nombre:string, imagen:string, imagenResponsive:string){
        this.id_juego = id_juego;
        this.nombre = nombre;
        this.imagen = imagen;
        this.imagenResponsive = imagenResponsive;
    }
}