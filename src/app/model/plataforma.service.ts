import { Inject, Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { Plataforma } from './plataforma';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class PlataformaService {
    constructor(private conexHttp: HttpClient) { }

    getPlataformas(lista_plataforma): Observable<any> {
        let url = "http://localhost/proyecto_m15_daw2/getPlataformas.php?plataformas=" + lista_plataforma;
        return this.conexHttp.get(url, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }
}