export class Tienda{
    id_tienda:number;
    nombre:string;
    imagen:string;

    constructor(id_tienda:number, nombre:string, imagen:string){
        this.id_tienda = id_tienda;
        this.nombre = nombre;
        this.imagen = imagen;
    }
}