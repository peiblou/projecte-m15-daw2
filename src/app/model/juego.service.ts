import { Inject, Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { Juego } from './juego';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class JuegoService {
    constructor(private conexHttp: HttpClient) { }
    public juegos: Array<Juego> = [];

    getJuegos(): Observable<any> {
        let url = "http://localhost/proyecto_m15_daw2/juegos.php";
        return this.conexHttp.get(url, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }

    getJuegosFilter(filtro): Observable<any> {
        let url = "http://localhost/proyecto_m15_daw2/getJuegosFilter.php?filtro=" + filtro;
        return this.conexHttp.get(url, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }

    getJuegosByPlataforma(plataforma): Observable<any> {
        let url = "http://localhost/proyecto_m15_daw2/getJuegosByPlataforma.php?plataforma=" + plataforma;
        return this.conexHttp.get(url, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }

    getJuego(juego): Observable<any> {
        console.log(juego);
        let url = "http://localhost/proyecto_m15_daw2/getJuego.php?juego=" + juego;
        return this.conexHttp.get(url, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }

    getRelacion(id): Observable<any> {
        let url = "http://localhost/proyecto_m15_daw2/getRelacion.php?id=" + id;
        return this.conexHttp.get(url, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }

    getPrecio(urlJuego): Observable<any> {
        let url = "http://localhost/proyecto_m15_daw2/scraper.php?url=" + urlJuego;
        return this.conexHttp.get(url, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }
}