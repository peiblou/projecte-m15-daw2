import { Inject, Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { Tienda } from './tienda';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class TiendaService {
    constructor(private conexHttp: HttpClient) { }

    getTiendas(tiendas): Observable<any> {
        let url = "http://localhost/proyecto_m15_daw2/tienda.php?tiendas=" + tiendas;
        return this.conexHttp.get(url, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }
}