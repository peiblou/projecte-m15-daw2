import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { eliminarComponent } from './deletecount/deletecount.component';

//import { IndexComponent } from './index/index.component';
import { inicioComponent } from './inicio/inicio.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { terminosComponent } from './terminos/terminos.component';
import { UpdatepasswordComponent } from './updatepassword/updatepassword.component';
import { updateperfilComponent } from './updateperfil/updateperfil.componet';
import { PaginaJuegoComponent } from './pagina-juego/pagina-juego.component';
import { BusquedaJuegoComponent } from './busqueda-juego/busqueda-juego.component';
import { JuegosPlataformaComponent } from './juegos-plataforma/juegos-plataforma.component';
import { ListaDeseadosComponent } from './lista-deseados/lista-deseados.component';

const routes: Routes = [
  { path: '', component: inicioComponent },
  { path: 'registrate', component: RegistroComponent },
  { path: 'iniciar', component: LoginComponent },
  { path: 'inicio', component: inicioComponent },
  { path: 'updateperfil', component: updateperfilComponent },
  { path: 'updatepassword', component: UpdatepasswordComponent },
  { path: 'terminos', component: terminosComponent },
  { path: 'eliminar', component: eliminarComponent },
  { path: 'juego/:juego', component: PaginaJuegoComponent },
  { path: 'busqueda/:filtro', component: BusquedaJuegoComponent },
  { path: 'plataforma/:plataforma', component: JuegosPlataformaComponent },
  { path: 'lista_deseados', component: ListaDeseadosComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
