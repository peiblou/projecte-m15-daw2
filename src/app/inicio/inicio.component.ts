import { Component, HostListener, OnInit } from '@angular/core';
import { Juego } from '../model/juego';
import { JuegoService } from '../model/juego.service';

@Component({
    selector: 'app-inicio',
    templateUrl: './inicio.component.html',
    styleUrls: ['./inicio.component.css'],
    providers: [JuegoService]
})
export class inicioComponent implements OnInit {
    lista_juegos: Array<Juego>;
    responsive = false;
    logueado = localStorage.getItem("token");

    constructor(private _juegoService: JuegoService) { }

    ngOnInit(): void {
        this._juegoService.getJuegos()
        this._juegoService.getJuegos()
        .subscribe(
            (result) => {
                this.lista_juegos = result;
                
                if (screen.width < 992) {
                    this.responsive = true;
                }
            },
            (error) => {
                console.log(error);
            }
        );


    }

    cerrarSesion() {
        localStorage.removeItem("token");
    }
    @HostListener('mouseover', ['$event.target']) onHover(evt) {
        if (evt.classList.contains('imgJuego')) {
            evt.style = "filter: brightness(70%);";
        }
    }

    @HostListener('mouseout', ['$event.target']) onOut(evt) {
        if (evt.classList.contains('imgJuego')) {
            evt.style = "filter: brightness(100%);";
        }
    }



}