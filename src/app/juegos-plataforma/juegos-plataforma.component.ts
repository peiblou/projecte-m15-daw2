import { Component, HostListener, OnInit } from '@angular/core';
import { Juego } from '../model/juego';
import { JuegoService } from '../model/juego.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-juegos-plataforma',
  templateUrl: './juegos-plataforma.component.html',
  styleUrls: ['./juegos-plataforma.component.css'],
  providers: [JuegoService]
})
export class JuegosPlataformaComponent implements OnInit {

  id_plataforma;
  lista_juegos;
  responsive = false;
  data = false;

  constructor(private _actRoute:ActivatedRoute, private _juegoService: JuegoService) { }

  ngOnInit(): void {
    this._actRoute.paramMap.subscribe(
      (params) => {
        this.id_plataforma = params.get("plataforma");
      }
    );

    this._juegoService.getJuegosByPlataforma(this.id_plataforma)
      .subscribe(
          (result) => {
              this.lista_juegos = result;

              if (screen.width < 992) {
                  this.responsive = true;
              }

              this.data = true;

          },
          (error) => {
              console.log(error);
          }
      );
  }

  cerrarSesion() {
    localStorage.removeItem("token");
  }
  @HostListener('mouseover', ['$event.target']) onHover(evt) {
      if (evt.classList.contains('imgJuego')) {
          evt.style = "filter: brightness(70%);";
      }
  }

  @HostListener('mouseout', ['$event.target']) onOut(evt) {
      if (evt.classList.contains('imgJuego')) {
          evt.style = "filter: brightness(100%);";
      }
  }
}


