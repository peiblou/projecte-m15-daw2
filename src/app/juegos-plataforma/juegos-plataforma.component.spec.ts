import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JuegosPlataformaComponent } from './juegos-plataforma.component';

describe('JuegosPlataformaComponent', () => {
  let component: JuegosPlataformaComponent;
  let fixture: ComponentFixture<JuegosPlataformaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JuegosPlataformaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JuegosPlataformaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
