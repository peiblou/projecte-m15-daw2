import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { updateperfilService } from './updateperfil.service';

@Component({
    selector: 'app-root',
    templateUrl: './updateperfil.componet.html',
    styleUrls: ['./updateperfil.componet.css']
})
export class updateperfilComponent {
    title = 'practica06Angular';
    update = {
        contraseya: "",
        nombre: "",
        telefono: ""
    }
    logueado = localStorage.getItem("token")

    constructor(private UpdateService: updateperfilService, private http: HttpClient, private router: Router) { }

    ngOnInit(): void {
        if (this.logueado == null) {
            window.location.href = "/";
        }
    }

    cerrarSesion() {
        localStorage.removeItem("token");
    }

    updatePerfil() {
        this.UpdateService.updatePerfil(this.update).subscribe(
            datos => {
                if (datos['resultado'] == 'OK') {
                    Swal.fire('Hey user!', 'Perfil Actulizado!', 'info');
                    Swal.update({
                        icon: 'success'
                    });
                } else {
                    if (datos['resultado'] == 'FAIL') {
                        console.log("no");
                        Swal.fire('Hey user!', 'Error este Nombre ya existe!', 'info');
                        Swal.update({
                            icon: 'error'
                        });
                    }
                    if (datos['resultado'] == 'FAIL1') {
                        Swal.fire('Hey user!', 'Este Telefono Ya existe!', 'info');
                        Swal.update({
                            icon: 'error'
                        });
                    }
                    if (datos['resultado'] == 'FAIL3') {
                        Swal.fire('Hey user!', 'Campos Vacios!', 'info');
                        Swal.update({
                            icon: 'error'
                        });
                    }
                    if (datos['resultado'] == 'FAIL4') {
                        Swal.fire('Hey user!', 'Contraseña incorrecta!', 'info');
                        Swal.update({
                            icon: 'error'
                        });
                    }
                }
            }
        );
    }

}
