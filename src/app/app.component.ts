import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProjecteM15DAW2';
  logueado;
  token;
  contenidoBarraBusqueda;

  constructor( private _actRoute:ActivatedRoute, private router:Router){}

  ngOnInit(): void {
  }

  loguin() {
    this.logueado = localStorage.getItem("token");
    if (this.logueado == null) {
      return false;
    } else {
      return true;
    }
  }

  cerrarSesion() {
    localStorage.removeItem("token");
  }
  Search(event: any){
    this.contenidoBarraBusqueda = event.target.value;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(["busqueda/"+this.contenidoBarraBusqueda]);
    }); 
  }

  buscarJuegosPlataforma(id_plataforma){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(["plataforma/"+id_plataforma]);
    }); 
  }
}
